package com.xreader.main;

import com.xreader.entities.Client;
import com.xreader.entities.Transaction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class ReadXMlCommandTest {

    @Autowired
    private ReadXMlCommand readXMlCommand;

    @Autowired
    private EntityManager entityManager;

    private List<Transaction> expectedTrans;

    @BeforeEach
    private void beforeAll() {
        expectedTrans = List.of(
                new Transaction("A PLACE 1", new BigDecimal("10.01"), "UAH", "123456****1234", createClient()),
                new Transaction("A PLACE 2", new BigDecimal("9876.01"), "UAH", "123456****1234", createClient()),
                new Transaction("A PLACE 3", new BigDecimal("12.01"), "USD", "123456****1234", createClient())
        );
    }

    private Client createClient() {
        return new Client("Ivan", "Ivanoff", "Ivanoff", 1234567890);
    }

    @Test
    void testReadXml() {
        readXMlCommand.uploadXml();
        var result = loadAllTrans();
        assertThat(result).containsAll(expectedTrans);
    }

    private List<Transaction> loadAllTrans() {
        return entityManager.createQuery("from Transaction").getResultList();
    }

}
