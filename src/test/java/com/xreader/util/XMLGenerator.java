package com.xreader.util;

import java.io.FileWriter;
import java.io.IOException;

public class XMLGenerator {

    public static void main(String[] args) {
        try {
            FileWriter writer = new FileWriter("big_XML.xml");

            head(writer);

            for (int i = 0; i < 400_000; i++) {
                writer.write("<transaction>");

                writer.write("<place>PLACE</place>");
                writer.write("<amount>12.01</amount>");
                writer.write("<currency>EUR</currency>");
                writer.write("<card>123456****1234</card>");

                writer.write("<client>");

                writer.write("<firstName>Ivan</firstName>");
                writer.write("<lastName>Sidoroff</lastName>");
                writer.write("<middleName>Sidoroff</middleName>");
                writer.write("<inn>1234567892</inn>");

                writer.write("</client>");

                writer.write("</transaction>");
            }

            teil(writer);

            writer.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    private static void teil(FileWriter writer) throws IOException {
        writer.write("</transactions>");
        writer.write("</ns2:GetTransactionsResponse>");
        writer.write("</soap:Body>");
        writer.write("</soap:Envelope>");
    }

    private static void head(FileWriter writer) throws IOException {
        writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
        writer.write("<soap:Body>");
        writer.write("<ns2:GetTransactionsResponse xmlns:ns2=\"http://dbo.qulix.com/ukrsibdbo\">");
        writer.write("<transactions>");
    }

}
