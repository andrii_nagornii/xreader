package com.xreader.reader;

import com.xreader.entities.Client;
import com.xreader.entities.Transaction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class StaxXMLReaderTest {

    private List<Transaction> expectedTrans;

    @BeforeEach
    private void beforeAll() {
        expectedTrans = List.of(
                new Transaction("A PLACE 1", new BigDecimal("10.01"), "UAH", "123456****1234", createClient()),
                new Transaction("A PLACE 2", new BigDecimal("9876.01"), "UAH", "123456****1234", createClient()),
                new Transaction("A PLACE 3", new BigDecimal("12.01"), "USD", "123456****1234", createClient())
        );
    }

    private Client createClient() {
        return new Client("Ivan", "Ivanoff", "Ivanoff", 1234567890);
    }

    @Test
    @DisplayName("Valid XML")
    void testValidXML() throws IOException, XMLStreamException {
        XMLEventReader eventReader = getEventReader("test_valid.xml");
        var xmlReader = new StaxXMLReader(eventReader, 10);
        var result = xmlReader.readTransactions();
        assertThat(result).containsAll(expectedTrans);
    }

    @Test
    @DisplayName("Empty XML")
    void testEmptyXML() throws IOException, XMLStreamException {
        XMLEventReader eventReader = getEventReader("test_empty.xml");
        var xmlReader = new StaxXMLReader(eventReader, 10);
        var result = xmlReader.readTransactions();
        assertThat(result).isEmpty();
    }

    @Test
    @DisplayName("Iterative XML reading")
    void testIterativeXML() throws IOException, XMLStreamException {
        XMLEventReader eventReader = getEventReader("test_iterative.xml");
        var xmlReader = new StaxXMLReader(eventReader, 2);

        var iteration_0 = xmlReader.readTransactions();
        var iteration_1 = xmlReader.readTransactions();
        var iteration_2 = xmlReader.readTransactions();
        var iteration_3 = xmlReader.readTransactions();
        var iteration_4 = xmlReader.readTransactions();

        assertThat(iteration_0).hasSize(2);
        assertThat(iteration_1).hasSize(2);
        assertThat(iteration_2).hasSize(2);
        assertThat(iteration_3).isEmpty();
        assertThat(iteration_4).isEmpty();
    }

    @Test
    @DisplayName("Test absent data in transaction")
    void testAbsentTransaction() throws IOException, XMLStreamException {
        XMLEventReader eventReader = getEventReader("test_absent_transaction_prop.xml");
        var xmlReader = new StaxXMLReader(eventReader, 10);

        Exception exception = assertThrows(IllegalArgumentException.class, xmlReader::readTransactions);

        assertThat(exception.getMessage()).contains("Absent transaction data");
    }

    @Test
    @DisplayName("Test correct tags values")
    void testValidationXML() throws IOException, XMLStreamException {
        XMLEventReader eventReader = getEventReader("test_broken.xml");
        var xmlReader = new StaxXMLReader(eventReader, 10);

        Exception exception = assertThrows(IllegalArgumentException.class, xmlReader::readTransactions);

        assertThat(exception.getMessage()).contains("Broken xml");
    }

    private XMLEventReader getEventReader(String path) throws XMLStreamException, IOException {
        var xmlInputFactory = XMLInputFactory.newInstance();
        var resource = new ClassPathResource(path);
        return xmlInputFactory.createXMLEventReader(resource.getInputStream());
    }
}
