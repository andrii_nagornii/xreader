package com.xreader.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)//for batch update
    private long id;

    @Column
    private String place;

    @Column
    private BigDecimal amount;

    @Column
    private String currency;

    @Column
    private String card;

    @ManyToOne
    @JoinColumn(name = "clientId")
    private Client client;

    public Transaction() {

    }

    public Transaction(String place, BigDecimal amount, String currency, String card, Client client) {
        this.place = place;
        this.amount = amount;
        this.currency = currency;
        this.card = card;
        this.client = client;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;

        return place.equals(that.place) &&
                amount.equals(that.amount) &&
                currency.equals(that.currency) &&
                card.equals(that.card) &&
                client.equals(that.client);
    }

    @Override
    public int hashCode() {
        return Objects.hash(place, amount, currency, card, client);
    }
}
