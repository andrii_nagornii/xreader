package com.xreader;

import com.xreader.main.ReadXMlCommand;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XReaderApplication {

	public static void main(String[] args) {
		var xmlReader = SpringApplication.run(XReaderApplication.class, args).getBean(ReadXMlCommand.class);

		if (args.length == 0)
			xmlReader.uploadXml();
		else if ("queued".equals(args[0])) {
			xmlReader.uploadXmlQueued();
		}
	}

}
