package com.xreader.conf;

import com.xreader.reader.StaxXMLReader;
import com.xreader.reader.XMLReader;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class Config {

    @Value("${batch-size:200}")
    private int batchSize;

    @Value("${file-path}")
    private String filePath;

    @Value("${db-url}")
    private String dbUrl;

    @Value("${db-driver}")
    private String dbDriver;

    @Value("${db-user}")
    private String dbUser;

    @Value("${db-password}")
    private String dbPassword;

    @Bean
    public DataSource dataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(dbDriver);
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(dbUser);
        dataSource.setPassword(dbPassword);

        return dataSource;
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan("com.xreader.entities");
        sessionFactory.setHibernateProperties(hibernateProperties());

        return sessionFactory;
    }

    @Bean
    public EntityManager entityManager() {
        return sessionFactory().getObject().createEntityManager();
    }

    @Bean
    public PlatformTransactionManager hibernateTransactionManager() {
        HibernateTransactionManager transactionManager
                = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory().getObject());

        return transactionManager;
    }

    private Properties hibernateProperties() {
        Properties hibernateProperties = new Properties();

        hibernateProperties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
        hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        hibernateProperties.setProperty("hibernate.jdbc.batch_size", String.valueOf(batchSize));

        return hibernateProperties;
    }

    @Bean
    public XMLReader xmlReader() throws FileNotFoundException, XMLStreamException {
        var xmlInputFactory = XMLInputFactory.newInstance();
        var eventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(filePath));

        return new StaxXMLReader(eventReader);
    }

}
