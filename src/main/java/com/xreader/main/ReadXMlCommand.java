package com.xreader.main;

import com.xreader.entities.Transaction;
import com.xreader.reader.XMLReader;
import com.xreader.writer.TransWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

@Service
public class ReadXMlCommand {

    @Value("${queue-size:10000}")
    private int queueSize;

    @Value("${trans-buffer-size:1}")
    private int transBufferSize;

    @Value("${batch-size}")
    private int batchSize;

    @Value("${file-path}")
    private String filePath;

    @Value("${db-url}")
    private String dbUrl;

    @Autowired
    private XMLReader xmlReader;

    @Autowired
    private TransWriter transWriter;

    @Autowired
    private EntityManager entityManager;

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_YELLOW = "\u001B[33m";

    public void uploadXml() {
        var transaction = entityManager.getTransaction();
        transaction.begin();

        try {
            System.out.println(ANSI_YELLOW + "= Start execution =" + ANSI_RESET);
            var currentTime = (double) System.currentTimeMillis();

            var trans = xmlReader.readTransactions();
            var progress = 0;
            while (!trans.isEmpty()) {
                transWriter.write(trans);
                trans = xmlReader.readTransactions();
                System.out.printf("Chunk count : %d \r", progress);
                progress++;
            }

            transaction.commit();
            System.out.println("Statistics : ");
            System.out.printf("time : %f mc \n", (System.currentTimeMillis() - currentTime) / 1000);

        } catch (Exception e) {
            transaction.rollback();
            e.printStackTrace();
        }
    }

    public void uploadXmlQueued() {
        var queue = new ArrayBlockingQueue<List<Transaction>>(queueSize);

        new Thread(new Writer(queue, entityManager, transWriter)).start();

        new Thread(new Reader(queue, xmlReader)).start();
    }

}

/**
 * Representing thread for reading XML
 */
class Reader implements Runnable {

    private final BlockingQueue<List<Transaction>> queue;
    private final XMLReader xmlReader;

    public Reader(BlockingQueue<List<Transaction>> queue, XMLReader xmlReader) {
        this.queue = queue;
        this.xmlReader = xmlReader;
    }

    @Override
    public void run() {
        var chunk = 0;
        while (true) {
            var transactions = xmlReader.readTransactions();
            System.out.printf("Chunk : %d ; Queue size : %d \r", chunk, queue.size());
            try {
                queue.put(transactions);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
            chunk++;

            if (transactions.isEmpty())
                break;
        }
    }
}

/**
 * Representing thread for writing to DB
 */
class Writer implements Runnable {

    private final BlockingQueue<List<Transaction>> queue;
    private final EntityManager entityManager;
    private final TransWriter transWriter;

    public Writer(BlockingQueue<List<Transaction>> queue, EntityManager entityManager, TransWriter transWriter) {
        this.queue = queue;
        this.entityManager = entityManager;
        this.transWriter = transWriter;
    }

    @Override
    public void run() {
        var currentTime = (double) System.currentTimeMillis();

        var transaction = entityManager.getTransaction();
        transaction.begin();

        while (true) {
            try {
                List<Transaction> transactions = queue.take();
                if (transactions.isEmpty())
                    break;

                transWriter.write(transactions);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }

        }

        transaction.commit();
        System.out.println();
        System.out.printf("time : %f mc \n", (System.currentTimeMillis() - currentTime) / 1000);
    }
}
