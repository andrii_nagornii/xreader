package com.xreader.writer;

import com.xreader.entities.Client;
import com.xreader.entities.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SimpleTransWriter implements TransWriter {

    @Autowired
    private EntityManager entityManager;

    @Value("${batch-size:200}")
    private int batchSize;

    private final Map<Long, Client> clientMap = new HashMap<>();

    public SimpleTransWriter() {
    }

    @Override
    public void write(List<Transaction> trans) {
        for (var i = 0; i < trans.size(); i++) {
            if (i > 0 && i % batchSize == 0) {
                entityManager.flush();
                entityManager.clear();
            }

            var client = trans.get(i).getClient();
            var inn = client.getInn();
            var clientInHash = clientMap.get(inn);

            if (clientInHash != null)
                client.setId(clientInHash.getId());
            else {
                entityManager.persist(client);
                clientMap.putIfAbsent(inn, client);
            }

            entityManager.persist(trans.get(i));
        }
    }
}
