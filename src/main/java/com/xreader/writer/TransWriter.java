package com.xreader.writer;

import com.xreader.entities.Transaction;

import java.util.List;

public interface TransWriter {

    void write(List<Transaction> transactions);

}
