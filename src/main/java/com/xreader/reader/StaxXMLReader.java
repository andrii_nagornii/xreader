package com.xreader.reader;

import com.xreader.entities.Client;
import com.xreader.entities.Transaction;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Map.of;

@Service
public class StaxXMLReader implements XMLReader {

    private static final String TRANSACTION = "transaction";

    private static final String PLACE = "place";
    private static final String AMOUNT = "amount";
    private static final String CURRENCY = "currency";
    private static final String CARD = "card";

    private static final String CLIENT = "client";

    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String MIDDLE_NAME = "middleName";
    private static final String INN = "inn";

    @Value("${trans-buffer-size:10}")
    private int transBuffer;

    private XMLEventReader reader;

    public StaxXMLReader() {
    }

    public StaxXMLReader(XMLEventReader reader, int transBuffer) {
        this.reader = reader;
        this.transBuffer = transBuffer;
    }

    public StaxXMLReader(XMLEventReader reader) {
        this.reader = reader;
    }

    @Override
    public List<Transaction> readTransactions() {

        try {
            var result = new LinkedList<Transaction>();
            var chunk = 0;
            while (reader.hasNext()) {
                var nextEvent = reader.nextEvent();

                if (nextEvent.isStartElement()) {
                    StartElement startElement = nextEvent.asStartElement();

                    if (startElement.getName().getLocalPart().equals(TRANSACTION)) {
                        result.add(readTransaction());

                        if (chunk == transBuffer - 1)
                            break;

                        chunk++;
                    }
                }
            }

            return result;

        } catch (XMLStreamException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Transaction readTransaction() throws XMLStreamException {
        var tranValidationMap = prepareEmptyValidationMap();

        var result = new Transaction();

        while (reader.hasNext()) {
            var nextEvent = reader.nextEvent();

            if (nextEvent.isStartElement()) {
                var startElement = nextEvent.asStartElement();
                var tagName = startElement.getName().getLocalPart();

                switch (tagName) {
                    case PLACE:
                        nextEvent = reader.nextEvent();
                        if (tranValidationMap.get(PLACE))
                            throw new XMLStreamException("more then one " + PLACE);

                        var place = nextEvent.asCharacters().getData();
                        tranValidationMap.put(PLACE, TRUE);
                        result.setPlace(place);
                        break;
                    case AMOUNT:
                        nextEvent = reader.nextEvent();
                        if (tranValidationMap.get(AMOUNT))
                            throw new XMLStreamException("more then one " + AMOUNT);

                        tranValidationMap.put(AMOUNT, TRUE);
                        var amount = nextEvent.asCharacters().getData();
                        result.setAmount(new BigDecimal(amount));
                        break;
                    case CURRENCY:
                        nextEvent = reader.nextEvent();
                        if (tranValidationMap.get(CURRENCY))
                            throw new XMLStreamException("more then one " + CURRENCY);

                        tranValidationMap.put(CURRENCY, TRUE);
                        var currency = nextEvent.asCharacters().getData();
                        result.setCurrency(currency);
                        break;
                    case CARD:
                        nextEvent = reader.nextEvent();
                        if (tranValidationMap.get(CARD))
                            throw new XMLStreamException("more then one " + CARD);

                        tranValidationMap.put(CARD, TRUE);
                        var card = nextEvent.asCharacters().getData();
                        result.setCard(card);
                        break;

                    case CLIENT:
                        result.setClient(readClient());
                        break;
                    default:
                        throw new XMLStreamException("Broken xml");
                }
            } else if (nextEvent.isEndElement()) {
                EndElement endElement = nextEvent.asEndElement();
                if (endElement.getName().getLocalPart().equals(TRANSACTION))
                    break;
            }

        }

        validateMap(tranValidationMap);
        return result;
    }

    private Map<String, Boolean> prepareEmptyValidationMap() {
        return new HashMap<>(of(
                PLACE, FALSE,
                AMOUNT, FALSE,
                CURRENCY, FALSE,
                CARD, FALSE
        ));
    }

    private Client readClient() throws XMLStreamException {
        var result = new Client();
        while (reader.hasNext()) {
            var nextEvent = reader.nextEvent();

            if (nextEvent.isStartElement()) {
                var startElement = nextEvent.asStartElement();
                var tagName = startElement.getName().getLocalPart();

                switch (tagName) {
                    case FIRST_NAME:
                        nextEvent = reader.nextEvent();
                        result.setFirstName(nextEvent.asCharacters().getData());
                        break;
                    case LAST_NAME:
                        nextEvent = reader.nextEvent();
                        result.setLastName(nextEvent.asCharacters().getData());
                        break;
                    case MIDDLE_NAME:
                        nextEvent = reader.nextEvent();
                        result.setMiddleName(nextEvent.asCharacters().getData());
                        break;
                    case INN:
                        nextEvent = reader.nextEvent();
                        var innAsStr = nextEvent.asCharacters().getData();
                        result.setInn(Long.parseLong(innAsStr));
                        break;
                    default:
                        throw new XMLStreamException("Wrong tag present!");
                }
            } else if (nextEvent.isEndElement()) {
                EndElement endElement = nextEvent.asEndElement();
                if (endElement.getName().getLocalPart().equals(CLIENT))
                    break;
            }

        }

        return result;
    }

    public void setReader(XMLEventReader reader) {
        this.reader = reader;
    }

    public void setTransBuffer(int transBuffer) {
        this.transBuffer = transBuffer;
    }

    private void validateMap(Map<String, Boolean> map) throws XMLStreamException {
        var result = map.values().stream().allMatch(b -> b);
        if (!result)
            throw new XMLStreamException("Absent transaction data");
    }

}
