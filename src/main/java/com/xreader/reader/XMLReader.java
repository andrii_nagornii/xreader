package com.xreader.reader;

import com.xreader.entities.Transaction;

import java.util.List;

public interface XMLReader {

    List<Transaction> readTransactions();

}
